import requests
import json
import matplotlib.pyplot as plt
import numpy as np

url = 'https://api.hkma.gov.hk/public/hkimr/food-price'

res = requests.get(url)

items = json.loads(res.text)['result']['records']

# print(items['result']['records'])

x = []
bread = []
tobac = []
coffee = []
milk = []
beef = []
pork = []
rice = []
tea = []
suger = []
salt = []

i = 0

for item in items:
    x.append(item['Fd_yr'])
    bread.append(item['Fd_bread_a'])
    tobac.append(item['Fd_tobac_a'])
    coffee.append(item['Fd_coffee_a'])
    milk.append(item['Fd_milk_a'])
    beef.append(item['Fd_beef_a'])
    pork.append(item['Fd_pork_a'])
    rice.append(item['Fd_rice_a'])
    tea.append(item['Fd_tea_a'])
    suger.append(item['Fd_sugar_a'])
    salt.append(item['Fd_salt_a'])
    i+=1
    if(i > 20):
        break

x = np.array(x)
bread = np.array(bread)
tobac = np.array(tobac)
coffee = np.array(coffee)
milk = np.array(milk)
beef = np.array(beef)
pork = np.array(pork)
rice = np.array(rice)
tea = np.array(tea)
suger = np.array(suger)
salt = np.array(salt)

plt.title("Food Price") # title
plt.ylabel("Price $") # y label
plt.xlabel("Year") # x label

plt.plot(x,bread, label='bread')
plt.plot(x,tobac, label='tobac')
plt.plot(x,coffee, label='coffee')
plt.plot(x,milk, label='milk')
plt.plot(x,beef, label='beef')
plt.plot(x,pork, label='pork')
plt.plot(x,tea, label='tea')
plt.plot(x,suger, label='suger')
plt.plot(x,salt, label='salt')
plt.legend(loc='best')

plt.show()

