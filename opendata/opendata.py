import pandas as pd

res = pd.read_csv('./opendata/statistics_on_daily_passenger_traffic.csv',encoding='utf-8')

print(res.describe)
print(res.mean())

with open('./opendata/statistic.csv', 'w') as f:
    f.write(res.describe().to_csv())