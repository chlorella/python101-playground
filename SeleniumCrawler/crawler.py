from selenium import webdriver
import requests
from bs4 import BeautifulSoup
from selenium.webdriver.firefox.options import Options

options = Options()
options.add_argument("--headless")

url = 'https://www.guruguruhk.com'

header = {
    'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
}

res = requests.get(url)

browser = webdriver.Firefox(options=options)
browser.get(url=url)

soup = BeautifulSoup(browser.page_source, 'html.parser')
items = soup.find_all('product-item')
# print(result)

data = []

for item in items:
    title = item.select('.title')[0].text.strip()
    price = item.select('.price').pop().text.strip()
    itemUrl = item.select('a')[0]['href']
    # print(title, price)
    data.append({
        'title' : title,
        'price' : price,
        'url' : url + itemUrl
    })

with open('guruguru.txt', 'w') as f:
    f.write(str(data))
