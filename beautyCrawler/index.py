import requests
import os
from bs4 import BeautifulSoup
from urllib.request import urlopen
header = {
    'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
}

res = requests.get('https://ithelp.ithome.com.tw/articles/10302577?sc=iThelpR', headers = header)
soup = BeautifulSoup(res.text, 'html.parser')

images = soup.select('div img') 

for image in images:
    if(image['src'] and 'imgur' in image['src']):
        filename = image['src'].split("/")[3]
        img = urlopen(image['src'])
        print(image['src'])
        with open('./images/' + str(filename), 'wb') as f:
            f.write(img.read())
 