from flask import Flask, render_template, request, redirect, url_for
from pytube import YouTube

app = Flask(__name__)

@app.route("/")
def index():
    filename = request.args['filename']
    return render_template('index.html', filename=filename)

@app.route("/submit", methods=["POST"])
def post_submit():
    url1 = request.form['url-1']
    YouTube(url1).streams.first().download()

    url2 = request.form['url-2']
    YouTube(url2).streams.first().download()

    filename1 = YouTube(url1).streams.first().default_filename
    filename2 = YouTube(url2).streams.first().default_filename

    return redirect(url_for('index', filename=(filename1 + ' and ' + filename2)))

if __name__ == '__main__':
    app.run(port=9000, debug=True)