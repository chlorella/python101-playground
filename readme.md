# Playground for Python Web 程式設計入門實戰（Python101）

A playground for [Python Web 程式設計入門實戰（Python101）](https://hahow.in/courses/58001218da04300700fdee95/main), code example initial from https://github.com/kdchang/python101-lite.

## Execute example

### Initial venv
```
$ python -m venv .
```

### install package
```
$ pip install package
```

### Run example
```
$ python3 index.py 
```