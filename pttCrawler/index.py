# https://www.ptt.cc/ask/over18
# /bbs/Gossiping/index.html
# https://www.ptt.cc/bbs/Gossiping/index.html

import requests
from bs4 import BeautifulSoup
from collections import defaultdict


payload = {
	'from': '/bbs/Gossiping/index.html',
	'yes': 'yes'
}

headers = {
	'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
}

rs = requests.Session()

rs.post('https://www.ptt.cc/ask/over18', data=payload, headers=headers)
res = rs.get('https://www.ptt.cc/bbs/Gossiping/index.html', headers=headers)

soup = BeautifulSoup(res.text, 'html.parser')

items = soup.select('.r-ent')
autherDict = defaultdict(list)

for item in items:
	#print('日期 {}'.format(item.select('.data')[0].text))
    auther = item.select('.author')[0].text.strip()
    title = item.select('.title')[0].text.strip()

    # print(item.select('.date')[0].text, item.select('.author')[0].text, item.select('.title')[0].text)
    autherDict[auther].append(title)

# print(autherDict)

for auther, pList in autherDict.items():
    print(auther, " : cnt", len(pList), ", title cnt: " , list(map( lambda p : len(p) ,pList)))

# Execise
## 1. image downlaoder
## 2. count author length of title